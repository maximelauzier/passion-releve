# Passion Releve Changelog

## [1.0.2] - 2018-09-10
* Switch to seothemes/core
* Fix return to top translation string
* Fix custom colors inline style handle

## [1.0.1] - 2018-09-01
* Fix core-widget-areas undefined index notices

## [1.0.0] - 2018-09-01
* Initial release
