/**
 * Custom JavaScript.
 */

(function (document, $) {
    'use strict';

    // Checks for existence in all browsers and IE 10/11 & Surface
    function is_touch_device() {
        return 'ontouchstart' in window || navigator.maxTouchPoints;
    }

    // Initialise flickity here
    if (!is_touch_device()) {
        var scroll = document.querySelector('.nav-secondary .menu');
        var flickity = new Flickity(scroll, {
            cellAlign: 'center',
            freeScroll: true,
            prevNextButtons: false,
            pageDots: false,
            contain: true
        });
    }

    // Toggle attr function.
    function _toggleAttr(attr, attr1, attr2) {
        return this.each(function () {
            var self = $(this);
            if (self.attr(attr) == attr1)
                self.attr(attr, attr2);
            else
                self.attr(attr, attr1);
        });
    };

    // Toggle search form on click.
    $('.search-toggle').on('click', function () {
        $('.header-widget-area .search-form').toggleClass('visible');
        $('.header-widget-area .search-form').fadeToggle('fast');
        $(this).toggleClass('activated');
        $(this).attr('aria-expanded', function (i, attr) {
            return attr == 'true' ? 'false' : 'true'
        });
        $(this).attr('aria-pressed', function (i, attr) {
            return attr == 'true' ? 'false' : 'true'
        });
    });

    // Add fitvids for gutenberg.
    $(".site-container").fitVids();

    // Add page top id for scroll to top.
    $('.site-container').attr('id', 'top');

    // Home top section.
    $('.home.blog .site-inner').not('.home.paged .site-inner').prepend('<div class="home-top"><div class="wrap"></div></div>');
    $('.home .entry').first().appendTo('.home-top .wrap');

    /**
     * Smooth scrolling.
     */
    // Select all links with hashes
    $('a[href*="#"]').not('[href*="#tab-"]').click(function (event) {

        // On-page links
        if (
            location.pathname.replace(/^\//, '') ==
            this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {

            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ?
                target :
                $('[name=' + this.hash.slice(1) + ']');

            // Does a scroll target exist?
            if (target.length) {

                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate(
                    {
                        scrollTop: target.offset().top
                    },
                    1000,
                    function () {

                        // Callback after animation, must change focus!
                        var $target = $(target);
                        $target.focus();

                        // Checking if the target was focused
                        if ($target.is(':focus')) {
                            return false;
                        } else {

                            // Adding tabindex for elements not focusable
                            $target.attr('tabindex', '-1');

                            // Set focus again
                            $target.focus();
                        }
                    }
                );
            }
        }
    });

}(document, jQuery));
