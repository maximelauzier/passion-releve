<?php
/**
 * Passion Releve
 *
 * @package   pr\pr
 * @link      https://maximelauzier.com/passion-releve
 * @author    Maxime Lauzier
 * @copyright Copyright © 2018 Maxime Lauzier
 * @license   GPL-3.0-or-later
 */

namespace pr\pr;

use SeoThemes\Core\AssetLoader;
use SeoThemes\Core\Constants;
use SeoThemes\Core\CustomColors;
use SeoThemes\Core\GenesisSettings;
use SeoThemes\Core\GoogleFonts;
use SeoThemes\Core\Hooks;
use SeoThemes\Core\ImageSizes;
use SeoThemes\Core\PageLayouts;
use SeoThemes\Core\PluginActivation;
use SeoThemes\Core\SimpleSocialIcons;
use SeoThemes\Core\TextDomain;
use SeoThemes\Core\ThemeSupport;
use SeoThemes\Core\WidgetArea;

$d2_assets = [
	/*AssetLoader::STYLES  => [
		[
			AssetLoader::HANDLE  => 'woocommerce',
			AssetLoader::URL     => AssetLoader::path( '/woocommerce.css' ),
			AssetLoader::VERSION => wp_get_theme()->get( 'Version' ),
			AssetLoader::ENQUEUE => true,
		],
	],*/
	AssetLoader::SCRIPTS => [
		[
			AssetLoader::HANDLE   => 'menus',
			AssetLoader::URL      => AssetLoader::path( '/resources/js/menus.js' ),
			AssetLoader::DEPS     => [ 'jquery' ],
			AssetLoader::VERSION  => wp_get_theme()->get( 'Version' ),
			AssetLoader::FOOTER   => true,
			AssetLoader::ENQUEUE  => true,
			AssetLoader::LOCALIZE => [
				AssetLoader::LOCALIZEVAR  => 'genesis_responsive_menu',
				AssetLoader::LOCALIZEDATA => [
					'mainMenu'         => __( '<span class="hamburger"></span>', 'passion-releve' ),
					'subMenu'          => __( 'Sub Menu', 'passion-releve' ),
					'menuIconClass'    => null,
					'subMenuIconClass' => null,
					'menuClasses'      => [
						'combine' => [
							'.nav-primary',
						],
					],
				]
			],
		],
		[
			AssetLoader::HANDLE  => 'flickity',
			AssetLoader::URL     => AssetLoader::path( '/resources/js/flickity.pkgd.js' ),
			AssetLoader::DEPS    => [ 'jquery' ],
			AssetLoader::VERSION => wp_get_theme()->get( 'Version' ),
			AssetLoader::FOOTER  => true,
			AssetLoader::ENQUEUE => true,
		],
		[
			AssetLoader::HANDLE  => 'fitvids',
			AssetLoader::URL     => AssetLoader::path( '/resources/js/jquery.fitvids.js' ),
			AssetLoader::DEPS    => [ 'jquery' ],
			AssetLoader::VERSION => wp_get_theme()->get( 'Version' ),
			AssetLoader::FOOTER  => true,
			AssetLoader::ENQUEUE => true,
		],
		[
			AssetLoader::HANDLE  => 'script',
			AssetLoader::URL     => AssetLoader::path( '/resources/js/script.js' ),
			AssetLoader::DEPS    => [ 'jquery', 'flickity', 'fitvids', 'menus' ],
			AssetLoader::VERSION => wp_get_theme()->get( 'Version' ),
			AssetLoader::FOOTER  => true,
			AssetLoader::ENQUEUE => true,
		],
		[
			AssetLoader::HANDLE  => 'direction',
			AssetLoader::URL     => AssetLoader::path( '/resources/js/direction.js' ),
			AssetLoader::DEPS    => [],
			AssetLoader::VERSION => wp_get_theme()->get( 'Version' ),
			AssetLoader::FOOTER  => true,
			AssetLoader::ENQUEUE => true,
		],
	],
];

$d2_constants = [
	Constants::DEFINE => [
		'CHILD_THEME_NAME'    => wp_get_theme()->get( 'Name' ),
		'CHILD_THEME_URL'     => wp_get_theme()->get( 'ThemeURI' ),
		'CHILD_THEME_VERSION' => wp_get_theme()->get( 'Version' ),
		'CHILD_THEME_HANDLE'  => wp_get_theme()->get( 'TextDomain' ),
		'CHILD_THEME_AUTHOR'  => wp_get_theme()->get( 'Author' ),
		'CHILD_THEME_DIR'     => get_stylesheet_directory(),
		'CHILD_THEME_URI'     => get_stylesheet_directory_uri(),
	],
];

$d2_custom_colors = [
	'background'        => [
		'default' => '#ffffff',
		'output'  => [
			[
				'elements'   => [
					'body',
					'.site-container',
				],
				'properties' => [
					'background-color' => '%s',
				],
			],
		],
	],
	'header_and_footer' => [
		'default' => '#000000',
		'output'  => [
			[
				'elements'   => [
					'html',
					'.site-header',
					'.site-footer',
				],
				'properties' => [
					'background-color' => '%s',
				],
			],
		],
	],
	'accent'            => [
		'default' => '#12be9f',
		'output'  => [
			[
				'elements'   => [
					'.nav-primary .menu-item a:hover',
					'.nav-primary .menu-item a:focus',
					'.genesis-nav-menu .current-menu-item > a',
					'.comment-content a:hover',
					'.comment-content a:focus',
					'.entry-content a:hover',
					'.entry-content a:focus',
					'.woocommerce ul.products li.product h3:hover',
					'.woocommerce ul.products li.product .price',
					'.woocommerce div.product p.price',
					'.woocommerce div.product span.price',
					'.woocommerce-error:before',
					'.woocommerce-error:before',
					'.woocommerce-error:before',
					'.woocommerce-info:before',
					'.woocommerce-info:before',
					'.woocommerce-info:before',
					'.woocommerce-message:before',
					'.woocommerce-message:before',
					'.woocommerce-message:before',
				],
				'properties' => [
					'color' => '%s',
				],
			],
			[
				'elements'   => [
					'button.alt',
					'input[type="button"].alt',
					'input[type="reset"].alt',
					'input[type="submit"].alt',
					'.button.alt',
					'button:focus',
					'button:hover',
					'input[type="button"]:focus',
					'input[type="button"]:hover',
					'input[type="reset"]:focus',
					'input[type="reset"]:hover',
					'input[type="submit"]:focus',
					'input[type="submit"]:hover',
					'.button:focus',
					'.button:hover',
					'.entry-categories a',
					'.entry-tags a',
					'.enews-widget input[type="submit"]',
					'.genesis-nav-menu a:before',
					'.wp-block-button .wp-block-button__link:not(.has-background):active',
					'.wp-block-button .wp-block-button__link:not(.has-background):focus',
					'.wp-block-button .wp-block-button__link:not(.has-background):hover',
					'.woocommerce span.onsale',
					'.woocommerce a.button:hover',
					'.woocommerce a.button:focus',
					'.woocommerce a.button.alt:hover',
					'.woocommerce a.button.alt:focus',
					'.woocommerce button.button:hover',
					'.woocommerce button.button:focus',
					'.woocommerce button.button.alt:hover',
					'.woocommerce button.button.alt:focus',
					'.woocommerce input.button:hover',
					'.woocommerce input.button:focus',
					'.woocommerce input.button.alt:hover',
					'.woocommerce input.button.alt:focus',
					'woocommerce input.button[type="submit"]:hover',
					'woocommerce input.button[type="submit"]:focus',
					'.woocommerce #respond input#submit:hover',
					'.woocommerce #respond input#submit:focus',
					'.woocommerce #respond input#submit.alt:hover',
					'.woocommerce #respond input#submit.alt:focus',
				],
				'properties' => [
					'background-color' => '%s',
				],
			],
			[
				'elements'   => [
					'a:hover',
					'a:focus',
					'.comment-content a',
					'.entry-content a',
				],
				'properties' => [
					'box-shadow' => 'inset 0 -0.1em 0 %s',
				],
			],
			[
				'elements'   => [
					'blockquote',
				],
				'properties' => [
					'border-left' => '3px solid %s',
				],
			],
			[
				'elements'   => [
					'.woocommerce-error',
					'.woocommerce-info',
					'.woocommerce-message',
				],
				'properties' => [
					'border-top-color' => '%s',
				],
			],
			[
				'elements'   => [
					'.site-title',
				],
				'properties' => [
					'border-bottom-color' => '%s',
				],
			],
		],
	],
];

$d2_genesis_settings = [
	GenesisSettings::DEFAULTS => [
		GenesisSettings::SITE_LAYOUT => 'full-width-content',
	],
];

$d2_google_fonts = [
	GoogleFonts::ENQUEUE => [
		'Oswald:300,400,500,600,700',
	],
];

$d2_hooks = [
	Hooks::ADD    => [
		[
			Hooks::TAG      => 'wp_enqueue_scripts',
			Hooks::CALLBACK => 'genesis_enqueue_main_stylesheet',
			Hooks::PRIORITY => 99,
		],
		[
			Hooks::TAG      => 'wp_enqueue_scripts',
			Hooks::CALLBACK => function () {
				wp_deregister_script( 'superfish' );
				wp_deregister_script( 'superfish-args' );
			},
		],
		[
			Hooks::TAG      => 'body_class',
			Hooks::CALLBACK => function ( $classes ) {
				if ( is_home() || is_search() || is_author() || is_date() || is_category() || is_tag() || is_page_template( 'page_blog.php' ) ) {
					$classes[] = 'post-grid';
				}

				$classes[] = 'no-js';

				return $classes;

			},
		],
		[
			Hooks::TAG      => 'genesis_before',
			Hooks::CALLBACK => function () {
				?>
                <script>
                    //<![CDATA[
                    (function () {
                        var c = document.body.classList;
                        c.remove('no-js');
                        c.add('js');
                    })();
                    //]]>
                </script>
				<?php
			},
			Hooks::PRIORITY => 1,
		],
		[
			Hooks::TAG         => 'genesis_site_title',
			Hooks::CALLBACK    => 'the_custom_logo',
			Hooks::PRIORITY    => 0,
			Hooks::CONDITIONAL => function () {
				return has_custom_logo();
			}
		],
		[
			Hooks::TAG      => 'genesis_markup_title-area_close',
			Hooks::CALLBACK => function ( $close_html ) {
				if ( $close_html ) {
					ob_start();
					do_action( 'child_theme_after_title_area' );
					$close_html = $close_html . ob_get_clean();
				}

				return $close_html;
			}
		],
		[
			Hooks::TAG      => 'genesis_before',
			Hooks::CALLBACK => function () {
				$wraps = get_theme_support( 'genesis-structural-wraps' );
				foreach ( $wraps[0] as $context ) {
					add_filter( "genesis_structural_wrap-{$context}", function ( $output, $original ) use ( $context ) {
						$position = ( 'open' === $original ) ? 'before' : 'after';
						ob_start();
						do_action( "child_theme_{$position}_{$context}_wrap" );
						if ( 'open' === $original ) {
							return ob_get_clean() . $output;
						} else {
							return $output . ob_get_clean();
						}
					}, 10, 2 );
				}
			}
		],
		[
			Hooks::TAG      => 'genesis_search_form',
			Hooks::CALLBACK => function ( $form ) {
				if ( ! did_action( 'genesis_after_header' ) ) {
					$form = '<button class="search-toggle" aria-expanded="false" aria-pressed="false" aria-label="' . __( 'Toggle search form', 'passion-releve' ) . '"><span class="search-icon"></span></button>' . $form;
				}

				return $form;
			},
		],
		[
			Hooks::TAG      => 'genesis_post_info',
			Hooks::CALLBACK => function ( $post_info ) {
				if ( ! is_singular() ) {
					$post_info = 'Par [post_author_posts_link] | [post_date]';
				}

				return $post_info;
			}
		],
		[
			Hooks::TAG      => 'genesis_post_meta',
			Hooks::CALLBACK => function () {
				if ( ! is_singular() ) {
					$post_meta = '[post_categories before="" sep="" after=""]';
				} else {
					$post_meta = '[post_categories before="" sep="" after=""] [post_tags before="" sep="" after=""]';
				}

				return $post_meta;
			}
		],
		[
			Hooks::TAG         => 'genesis_entry_header',
			Hooks::CALLBACK    => 'genesis_post_meta',
			Hooks::PRIORITY    => 9,
			Hooks::CONDITIONAL => function () {
				return ! is_single();
			}
		],
		[
			Hooks::TAG      => 'genesis_attr_content-sidebar-wrap',
			Hooks::CALLBACK => function ( $atts ) {
				$atts['class'] = 'wrap';

				return $atts;
			},
		],
		[
			Hooks::TAG      => 'genesis_header',
			Hooks::CALLBACK => 'genesis_do_nav',
			Hooks::PRIORITY => 5,
		],
		[
			Hooks::TAG      => 'child_theme_after_header_wrap',
			Hooks::CALLBACK => 'genesis_do_subnav',
		],
		[
			Hooks::TAG      => 'child_theme_before_footer_wrap',
			Hooks::CALLBACK => 'genesis_footer_widget_areas',
		],
		[
			Hooks::TAG      => 'genesis_structural_wrap-footer',
			Hooks::CALLBACK => function ( $output, $original_output ) {
				if ( 'open' == $original_output ) {
					$output = '<div class="footer-credits">' . $output;
				} elseif ( 'close' == $original_output ) {
					$backtotop = '<a href="#" rel="nofollow" class="backtotop">' . __( 'Return to top', 'passion-releve' ) . '</a>';
					$output    = $backtotop . $output . $output;
				}

				return $output;
			},
			Hooks::PRIORITY => 10,
			Hooks::ARGS     => 2,
		],
		[
			Hooks::TAG      => 'genesis_entry_header',
			Hooks::CALLBACK => 'genesis_do_post_image',
			Hooks::PRIORITY => 1,
		],
		[
			Hooks::TAG      => 'genesis_entry_header',
			Hooks::PRIORITY => 15,
			Hooks::CALLBACK => function () {
				if ( ! is_singular( 'post' ) ) {
					return;
				}

				genesis_image();
			},
		],
		[
			Hooks::TAG      => 'simple_social_default_profiles',
			Hooks::CALLBACK => function ( $profiles ) {
				foreach ( $profiles as $profile => $args ) {
					$label                           = str_replace( ' URI', '', $args['label'] );
					$pattern                         = str_replace( '</a>', '<b class="ssi-label">' . $label . '</b></a>', $args['pattern'] );
					$profiles[ $profile ]['pattern'] = $pattern;
				}

				return $profiles;
			},
		],
		[
			Hooks::TAG      => 'genesis_prev_link_text',
			Hooks::CALLBACK => function () {
				return '← Précédent';
			},
		],
		[
			Hooks::TAG      => 'genesis_next_link_text',
			Hooks::CALLBACK => function () {
				return 'Suivant →';
			},
		],
		[
			Hooks::TAG      => 'wp_nav_menu_args',
			Hooks::CALLBACK => function ( $args ) {
				if ( 'secondary' === $args['theme_location'] ) {
					$args['depth'] = 1;
				}

				return $args;
			},
		],
		[
			Hooks::TAG      => 'genesis_markup_entry-title_open',
			Hooks::CALLBACK => function ( $open, $args ) {
				if ( ! empty( $args['params']['is_widget'] ) && ! did_action( 'genesis_before_sidebar_widget_area' ) ) {
					$open = do_shortcode( '[post_categories before="" sep="" after=""]' ) . $open;
				}

				return $open;
			},
			Hooks::ARGS     => 2,
		],
		[
			Hooks::TAG         => 'genesis_entry_header',
			Hooks::CALLBACK    => 'genesis_post_meta',
			Hooks::PRIORITY    => 1,
			Hooks::CONDITIONAL => function () {
				return is_single() && ! is_singular( 'product' );
			}
		],
	],
	Hooks::REMOVE => [
		[
			Hooks::TAG      => 'genesis_meta',
			Hooks::CALLBACK => 'genesis_load_stylesheet',
		],
		[
			Hooks::TAG      => 'genesis_after_header',
			Hooks::CALLBACK => 'genesis_do_nav',
		],
		[
			Hooks::TAG      => 'genesis_after_header',
			Hooks::CALLBACK => 'genesis_do_subnav',
		],
		[
			Hooks::TAG      => 'genesis_before_footer',
			Hooks::CALLBACK => 'genesis_footer_widget_areas',
		],
		[
			Hooks::TAG      => 'genesis_entry_content',
			Hooks::CALLBACK => 'genesis_do_post_image',
			Hooks::PRIORITY => 8,
		],
		[
			Hooks::TAG      => 'genesis_entry_footer',
			Hooks::CALLBACK => 'genesis_post_meta',
		],
		[
			Hooks::TAG         => 'genesis_entry_content',
			Hooks::CALLBACK    => 'genesis_do_post_content',
			Hooks::CONDITIONAL => function () {
				$limit = get_option( 'genesis-settings' )['content_archive_limit'];

				return ! is_singular() || is_page_template( 'page_blog.php' ) && ( 0 === $limit );
			}
		],
	],
];

$d2_image_sizes = [
	ImageSizes::ADD => [
		'featured' => [
			'width'  => 620,
			'height' => 380,
			'crop'   => true,
		],
	],
];

$d2_layouts = [
	PageLayouts::UNREGISTER => [
		PageLayouts::CONTENT_SIDEBAR_SIDEBAR,
		PageLayouts::SIDEBAR_SIDEBAR_CONTENT,
		PageLayouts::SIDEBAR_CONTENT_SIDEBAR,
	]
];

$d2_plugins = [
	PluginActivation::REGISTER => [
		[
			PluginActivation::NAME     => 'AMP',
			PluginActivation::SLUG     => 'amp',
			PluginActivation::REQUIRED => false,
		],
		[
			PluginActivation::NAME     => 'Genesis eNews Extended',
			PluginActivation::SLUG     => 'genesis-enews-extended',
			PluginActivation::REQUIRED => false,
		],
		[
			PluginActivation::NAME     => 'Gutenberg',
			PluginActivation::SLUG     => 'gutenberg',
			PluginActivation::REQUIRED => false,
		],
		[
			PluginActivation::NAME     => 'Shared Counts',
			PluginActivation::SLUG     => 'shared-counts',
			PluginActivation::REQUIRED => false,
		],
		[
			PluginActivation::NAME     => 'Simple Social Icons',
			PluginActivation::SLUG     => 'simple-social-icons',
			PluginActivation::REQUIRED => false,
		],
		[
			PluginActivation::NAME     => 'Widget Importer and Exporter',
			PluginActivation::SLUG     => 'widget-importer-exporter',
			PluginActivation::REQUIRED => false,
		],
		[
			PluginActivation::NAME     => 'WordPress Importer',
			PluginActivation::SLUG     => 'wordpress-importer',
			PluginActivation::REQUIRED => false,
		],
	],
];

if ( class_exists( 'WooCommerce' ) ) {
	$d2_plugins[ PluginActivation::REGISTER ][] = [
		PluginActivation::NAME     => 'Genesis Connect for WooCommerce',
		PluginActivation::SLUG     => 'genesis-connect-woocommerce',
		PluginActivation::REQUIRED => true,
	];
}

$d2_simple_social_icons = [
	SimpleSocialIcons::DEFAULTS => [
		SimpleSocialIcons::NEW_WINDOW => 1,
		SimpleSocialIcons::SIZE       => 40,
	],
];

$d2_textdomain = [
	TextDomain::DOMAIN => 'passion-releve',
];

$d2_theme_support = [
	ThemeSupport::ADD => [
		'align-wide'                  => null,
		'automatic-feed-links'        => null,
		'custom-logo'                 => [
			'height'      => 100,
			'width'       => 300,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => [
				'.site-title',
				'.site-description',
			],
		],
		'custom-header'               => [
			'header-selector' => '.hero-section',
			'default_image'   => get_stylesheet_directory_uri() . '/resources/img/hero.jpg',
			'header-text'     => false,
			'width'           => 1280,
			'height'          => 720,
			'flex-height'     => true,
			'flex-width'      => true,
			'uploads'         => true,
			'video'           => true,
		],
		'genesis-accessibility'       => [
			'404-page',
			'drop-down-menu',
			'headings',
			'rems',
			'search-form',
			'skip-links',
		],
		'genesis-after-entry-widget-area',
		'genesis-footer-widgets'      => 3,
		'genesis-menus'               => [
			'primary'   => __( 'Off Canvas Menu', 'passion-releve' ),
			'secondary' => __( 'After Header Menu', 'passion-releve' ),
		],
		'genesis-responsive-viewport' => null,
		'genesis-structural-wraps'    => [
			'header',
			'footer-widgets',
			'footer',
		],
		'gutenberg'                   => [
			'wide-images' => true,
		],
		'html5'                       => [
			'caption',
			'comment-form',
			'comment-list',
			'gallery',
			'search-form',
		],
		'post-thumbnails',
		'woocommerce'                 => null,
		'wc-product-gallery-zoom'     => null,
		'wc-product-gallery-lightbox' => null,
		'wc-product-gallery-slider'   => null,
		'wp-block-styles'             => null,
	],
];

$d2_widget_areas = [
	WidgetArea::REGISTER   => [
		[
			WidgetArea::ID          => 'before-content',
			WidgetArea::NAME        => __( 'Before Content', 'passion-releve' ),
			WidgetArea::DESCRIPTION => __( 'Displays a banner ad below the site header section.', 'passion-releve' ),
			WidgetArea::BEFORE      => '<div class="before-content widget-area">',
			WidgetArea::AFTER       => '</div>',
			WidgetArea::LOCATION    => 'genesis_before_content',
		],
		[
			WidgetArea::ID          => 'after-content',
			WidgetArea::NAME        => __( 'After Content', 'passion-releve' ),
			WidgetArea::DESCRIPTION => __( 'Displays a banner ad above the site footer section.', 'passion-releve' ),
			WidgetArea::BEFORE      => '<div class="after-content widget-area">',
			WidgetArea::AFTER       => '</div>',
			WidgetArea::LOCATION    => 'genesis_after_content',
		],
	],
	WidgetArea::UNREGISTER => [
		WidgetArea::SIDEBAR_ALT,
	],
];

return [
	AssetLoader::class       => $d2_assets,
	Constants::class         => $d2_constants,
	CustomColors::class      => $d2_custom_colors,
	GenesisSettings::class   => $d2_genesis_settings,
	GoogleFonts::class       => $d2_google_fonts,
	Hooks::class             => $d2_hooks,
	ImageSizes::class        => $d2_image_sizes,
	PageLayouts::class       => $d2_layouts,
	PluginActivation::class  => $d2_plugins,
	SimpleSocialIcons::class => $d2_simple_social_icons,
	TextDomain::class        => $d2_textdomain,
	ThemeSupport::class      => $d2_theme_support,
	WidgetArea::class        => $d2_widget_areas,
];
